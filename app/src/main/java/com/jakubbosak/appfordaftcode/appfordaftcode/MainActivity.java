package com.jakubbosak.appfordaftcode.appfordaftcode;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;


public class MainActivity extends AppCompatActivity {
    private String address;
    private String protocol;
    private String data;
    private Intent showPageSourceIntent;
    private String invalidURL = "It is a invalid URL";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pageSourceOnClick(View view) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        showPageSourceIntent = new Intent(this, PageSourceActivity.class);
        EditText addressText = (EditText) findViewById(R.id.activity_main_address);
        address = addressText.getText().toString();

        if (address.contains("http://")) {
            address = address.substring(7, address.length());
        }
        if (address.contains("https://")) {
            address = address.substring(8, address.length());
        }
        if (!isNetworkAvailable()) {
            loadingWithoutInternet();
        } else {
            loadingWithInternet();
        }
    }

    private void loadingWithInternet() {
        if (isHttps(address) == "https://") {
            protocol = isHttps(address);
            ActivatePageSource();
        } else if (isHttp(address) == "http://") {
            protocol = isHttp(address);
            ActivatePageSource();
        } else {
            Toast.makeText(getApplicationContext(), "Error: " + isHttp(address), Toast.LENGTH_SHORT).show();
        }

    }

    private void ActivatePageSource() {
        URL url = null;
        try {
            url = new URL(protocol + address);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        showPageSourceIntent.putExtra("address", url.toString());
        startActivity(showPageSourceIntent);
    }

    private String isHttps(String address) {
        int httpsResCode = 0;
        try {
            URL httpsURL = new URL("https://" + address);
            HttpsURLConnection httpsConn = (HttpsURLConnection) httpsURL.openConnection();
            httpsResCode = httpsConn.getResponseCode();
            if (httpsConn.getResponseCode() == 200) {
                return "https://";
            }

        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), invalidURL, Toast.LENGTH_SHORT).show();
        } catch (SSLPeerUnverifiedException e) {
            Toast.makeText(getApplicationContext(), invalidURL, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), invalidURL, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return String.valueOf(httpsResCode);
    }

    private String isHttp(String address) {
        int httpResCode = 0;
        try {
            URL httpURL = new URL("http://" + address);
            HttpURLConnection httpConn = (HttpURLConnection) httpURL.openConnection();
            httpResCode = httpConn.getResponseCode();
            if (httpResCode == 200) {
                return "http://";
            }
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), invalidURL, Toast.LENGTH_SHORT).show();
        } catch (SSLPeerUnverifiedException e) {
            Toast.makeText(getApplicationContext(), invalidURL, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), invalidURL, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        return String.valueOf(httpResCode);
    }

    private void loadingWithoutInternet() {
        String noInternetLoadMemory = "No internet connection,loading from memory";
        String noInternet = "Error, check internet connection";
        loadingFile("http://");
        if (data.contains("html")) {
            Toast.makeText(getApplicationContext(), noInternetLoadMemory, Toast.LENGTH_LONG).show();
            PuttingToIntent();
            startActivity(showPageSourceIntent);
        } else {
            loadingFile("https://");
            if (data.contains("html")) {
                Toast.makeText(getApplicationContext(), noInternetLoadMemory, Toast.LENGTH_LONG).show();
                PuttingToIntent();
                startActivity(showPageSourceIntent);
            }else{
                Toast.makeText(getApplicationContext(), noInternet, Toast.LENGTH_LONG).show();
            }

        }

    }

    private void PuttingToIntent() {
        showPageSourceIntent.putExtra("address", address);
        showPageSourceIntent.putExtra("data", data);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void loadingFile(String prot) {
        SharedPreferences preferences;
        preferences = getSharedPreferences("file", 0);
        data = preferences.getString(prot + address, "");
    }
}