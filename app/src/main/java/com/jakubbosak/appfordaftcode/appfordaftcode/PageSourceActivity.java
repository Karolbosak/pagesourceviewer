package com.jakubbosak.appfordaftcode.appfordaftcode;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;

public class PageSourceActivity extends AppCompatActivity {
    TextView sourceText;
    String address;
    String data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_source);
        gettingVariables();

        if (data !=null) {
            sourceText.setText(data);
        } else{
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
                sourceText.setText(getWebsite(address));
            } catch (MalformedURLException e) {
                Toast.makeText(getApplicationContext(), (CharSequence) e, Toast.LENGTH_LONG).show();
            } catch (ConnectException e) {
                Toast.makeText(getApplicationContext(), (CharSequence) e, Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), (CharSequence) e, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), (CharSequence) e, Toast.LENGTH_LONG).show();
            }
            savingToFile();
        }
    }

    private void gettingVariables() {
        sourceText = (TextView) findViewById(R.id.page_source_text);
        address = getIntent().getStringExtra("address");
        data = getIntent().getStringExtra("data");
    }

    private void savingToFile() {
        SharedPreferences preferences;
        preferences = getSharedPreferences("file", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(address, (String) sourceText.getText());
        editor.commit();
    }

    private String getWebsite(String website) throws IOException {
        String source = "";
        URL url = new URL(website);

        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
        String line;
        while ((line = in.readLine()) != null) {
            source += line + "\n";
        }
        in.close();
        return source;
    }

}
